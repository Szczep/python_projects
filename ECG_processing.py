# It's a part of my diploma project on my Bachelor studies. In the script I'm processing ECG signal imported 
# from Physio Net medical signals database in wfdb format. The project is showing possibilities of using open 
# source Python's libraries, intended for processing signals and especially ECG signal. The signal is filtered 
# with bandpass filter with cutoffs 1 Hz and 40 Hz and finding peaks with searching local maximum functions. 
from turtle import color
import PyCRC
from pyecg import ECGRecord
from matplotlib import pyplot as plt
import numpy as np
import scipy.signal
# import scipy.fft
from scipy.fft import fft, fftfreq, irfft, rfft, rfftfreq
from scipy.signal import butter, lfilter
from sklearn.preprocessing import StandardScaler
import neurokit2 as nk

#Extracting ECG signal/Ekstrakcja sygnału EKG z pliku

# To load a wfdb formatted ECG record
hea_path = r'C:\Users\sebex\Desktop\Projekty\Python_Projects3\Project4\rec_1.hea'
record = ECGRecord.from_wfdb(hea_path)
lead_name=record.lead_names
F_samp = 500.0 # ilosc probek na sekunde
T = len(record)/(F_samp) # calkowity czas
dt = 1/F_samp # krok czasowy
t = np.arange(0,T,dt) # tablica czasowa
f = np.arange(0.01,F_samp/2,0.01) # tablica czestotliwosciowa
# time = record.time
signal = record.get_lead('I')
signal_cl = record.get_lead('filtered') # pobrany sygnał przefiltrowany przez Physionet dla porównania
plt.figure(figsize=(10,10))
plt.plot(t[:1000],signal[:1000])
# plt.show()

#Cleaning data/Czyszczenie danych

nyq=0.5*F_samp # częstotliwość sygnału EKG 2 razy mniejsza od częstotliwośći próbkowania
lowcut=1 # dla 0.5 i 0.05 sygnał "pływa", więcej dałoby "gładszy" charakter sygnału, ale zabrałoby część informacji
highcut=40
low = lowcut/nyq
high = highcut/nyq
b, a = butter(N=2, Wn=[low, high], btype='bandpass') # N - rząd filtru, a,b - składowe wektory współczynników
y = lfilter(b, a, signal)
plt.figure(figsize=(10,10))
plt.plot(t[:1000],y[:1000])
 
#Finding R peaks/Znajdowanie peaków R

rpeaks, _=scipy.signal.find_peaks(y,prominence=0.6) # the function is finding local maximums
t_rpeaks=rpeaks/F_samp
plt.figure(figsize=(10,10))
plt.plot(t,y)
plt.scatter(t_rpeaks,y[rpeaks],color='red',marker="x")
# plt.show()

#Counting mean heart rate/Liczenie średniego tętna

intervals=[]
for i in range(len(t_rpeaks)-1):
    intervals.append(t_rpeaks[i+1]-t_rpeaks[i])
mean_interval=np.mean(intervals)
BPM=60/mean_interval
print("The mean heart rate of the current ECG signal is {} BPM".format(np.round(BPM)))

#Finding other characteristic peaks/Odnajdywanie innych charakterystycznych peaków
_, waves_peak = nk.ecg_delineate(y, rpeaks, sampling_rate=F_samp, method="peak", show=False, show_type='peaks')
plt.figure(figsize=(10,10))
plt.plot(t,y)
t_P_peaks=np.array(waves_peak['ECG_P_Peaks'])/F_samp
t_Q_peaks=np.array(waves_peak['ECG_Q_Peaks'])/F_samp
t_S_peaks=np.array(waves_peak['ECG_S_Peaks'])/F_samp
t_T_peaks=np.array(waves_peak['ECG_T_Peaks'])/F_samp
plt.scatter(t_rpeaks,y[rpeaks],color="red",label="R peaks")
plt.scatter(t_P_peaks,y[waves_peak['ECG_P_Peaks']],color="orange",label="P peaks")
plt.scatter(t_Q_peaks,y[waves_peak['ECG_Q_Peaks']],color="black",label="Q peaks")
plt.scatter(t_S_peaks,y[waves_peak['ECG_S_Peaks']],color="yellow",label="S peaks")
plt.scatter(t_T_peaks,y[waves_peak['ECG_T_Peaks']],color="green",label="T peaks")
plt.legend()

#Comparing periods/Porównywanie okresów

# modulo=len(y)%len(peaks)
# periods_signals=y[:-modulo].reshape(-1,len(peaks))
# periods_time=t[:-modulo].reshape(-1,len(peaks))
# print(periods_signals[peaks[0]])
# plt.figure(figsize=(10,10))
# for i in range(len(rpeaks)):
#     plt.plot(t[:len(y[(rpeaks[i]-100):(rpeaks[i]+100)])],y[(rpeaks[i]-100):(rpeaks[i]+100)])
plt.figure(figsize=(10,10))
halfinterval=int(mean_interval*F_samp//2)
for i in range(len(rpeaks)):
    period=y[(rpeaks[i]-halfinterval):(rpeaks[i]+halfinterval)]
    plt.plot(t[:len(period)],y[(rpeaks[i]-halfinterval):(rpeaks[i]+halfinterval)])
plt.show()

#FFT

hamming_window=np.hamming(len(y))
N=len(y)
hamming_window = hamming_window/np.linalg.norm(hamming_window)
y = y*hamming_window 
Y = rfft(y)
P = Y*Y.conj() # Oblicz moc jako iloczyn unormowanej transformaty i jej sprzężenia zespolonego. 
P = P/F_samp # Unormuj widmo dzieląc przez częstość próbkowania
P = P.real # Do dalszych operacji wybierz tylko część rzeczywistą mocy. 
#rfft obliczaja transformate dla dodatnniej czesci osi
if len(y)%2 ==0: # dokładamy moc z ujemnej części widma 
    P[1:-1] *=2
else:
    P[1:] *=2
F = rfftfreq(len(y), 1/F_samp) 
yf = fft(y)
xf = fftfreq(N, dt)
plt.figure(figsize=(10,10))
plt.plot(xf, np.abs(yf))
plt.figure(figsize=(10,10))
plt.stem(F, P)

#FFT for QRS complex/FFT dla zespołu QRS
qrs_distance=halfinterval//3


# fig, axs = plt.subplots(2, 2)
# axs[0, 0].plot(t, y)
# axs[1, 1].scatter(t, y)
plt.figure(figsize=(15,15))
for i in range(len(rpeaks)):
    qrs=y[(rpeaks[i]-qrs_distance):(rpeaks[i]+qrs_distance)]
    N=len(qrs)
    yf_qrs = fft(qrs)
    xf_qrs = fftfreq(N, dt)
    plt.stem(xf_qrs, np.abs(yf_qrs))
plt.show()

#Reverse FFT/Odwrócona FFT

rev_yf=irfft(yf)
plt.figure(figsize=(10,10))
plt.plot(rev_yf)
plt.show()