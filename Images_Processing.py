#It's my script from my laboratories, where we get instruction sheet with described lines of code, 
# which we need to use to accomplish our lesson
#1
import cv2 # biblioteka OpenCV <- znakiem # oznaczamy początek komentarza
import numpy as np # biblioteka numpy (wywoływana pod nazwą np)
import matplotlib.pyplot as plt # biblioteka matplotlib moduł 
 # wyświetlania obrazów (plt)
sciezka_do_zdjecia = r"C:\Users\sebex\Desktop\Projekty\Python_Projects3\Project3\obiekty_mono.tif" # ‘r’ tylko dla Windows
img = cv2.imread(sciezka_do_zdjecia) # format oryginalny
img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(img_gs, 200, 255, 0)
contours1, hierarchy1 = cv2.findContours(thresh, cv2.RETR_LIST,
 cv2.CHAIN_APPROX_NONE)

max=0
imax=0
for i in range(0,len(contours1)-1):
    contour = contours1[i]
    area1 = cv2.contourArea(contour)
    if max<area1:
        print(f"area1 {area1}")
        max=area1
        imax=i
print(max)
contour=contours1[imax]
background_white = True
out = np.ones(img.shape, dtype=np.uint8)
if background_white:
    out = out * 255
else:
    out = out * 0
x, y, w, h = cv2.boundingRect(contour)
for x1 in range(x, x + w):
    for y1 in range(y, y + h):
        out[y1][x1] = img[y1][x1]



plt.figure()
plt.title("obraz oryginalny")

imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
channels = 0 # wybór kanału kolorów
hist = cv2.calcHist([img], [channels], None, [256], [0, 256])
plt.figure('Histogram')
plt.xlabel("Grayscale value")
plt.ylabel("Pixel count")
plt.plot(hist, color='b')
plt.xlim([-1, 256])

plt.figure()
plt.title("kontury")
#  Rysowanie wszystkich konturów na czerwono, grubość linii 2
cv2.drawContours(img, contours1, -1, (0, 255, 0), 2)
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.figure()
plt.title("najwiekszy kontur")
#  Rysowanie wszystkich konturów na czerwono, grubość linii 2
cv2.drawContours(out, contours1, imax, (255, 0, 0), 2)
imgplot = plt.imshow(out) # wyświetlanie zdjęcia w formacie rgb
plt.show() # Wyświetlanie blokujące skrypt nie wykona kolejnych
 # operacji dopóki okno nie zostanie zamknięte

print("koniec zad1")

# #2
import cv2 # biblioteka OpenCV <- znakiem # oznaczamy początek komentarza
import numpy as np # biblioteka numpy (wywoływana pod nazwą np)
import matplotlib.pyplot as plt # biblioteka matplotlib moduł 
 # wyświetlania obrazów (plt)
sciezka_do_zdjecia = r"C:\Users\sebex\Desktop\Projekty\Python_Projects3\Project3\sprawdziany.tif" # ‘r’ tylko dla Windows
img = cv2.imread(sciezka_do_zdjecia) # format oryginalny
img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(img_gs, 160, 255, 0)
contours1, hierarchy1 = cv2.findContours(thresh, cv2.RETR_LIST,
 cv2.CHAIN_APPROX_NONE)

suma=0
srednia=0
for i in range(0,len(contours1)-1):
    contour = contours1[i]
    area1 = cv2.contourArea(contour)
    if area1>10000 and area1<12000:
        print(f"area1 {area1}")
        suma=suma+area1
print(suma)
srednia=suma/3
print("srednia: "+str(srednia))

plt.figure()
plt.title("obraz oryginalny")
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
channels = 0 # wybór kanału kolorów
hist = cv2.calcHist([img], [channels], None, [256], [0, 256])
plt.figure('Histogram')
plt.xlabel("Grayscale value")
plt.ylabel("Pixel count")
plt.plot(hist, color='b')
plt.xlim([-1, 256])
plt.figure()
plt.title("kontury")
#  Rysowanie wszystkich konturów na czerwono, grubość linii 2
cv2.drawContours(img, contours1, -1, (255, 0, 0), 2)
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.show()

print("koniec zad2")

 #3
import cv2 # biblioteka OpenCV <- znakiem # oznaczamy początek komentarza
import numpy as np # biblioteka numpy (wywoływana pod nazwą np)
import matplotlib.pyplot as plt # biblioteka matplotlib moduł 
 # wyświetlania obrazów (plt)
sciezka_do_zdjecia = r"C:\Users\sebex\Desktop\Projekty\Python_Projects3\Project3\figury_mono.tif" # ‘r’ tylko dla Windows
img = cv2.imread(sciezka_do_zdjecia) # format oryginalny
img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(img_gs, 200, 255, 0)
contours1, hierarchy1 = cv2.findContours(thresh, cv2.RETR_LIST,
 cv2.CHAIN_APPROX_NONE)

max=0
min=10000000
imax=0
imin=0
for i in range(0,len(contours1)-1):
    contour = contours1[i]
    area1 = cv2.contourArea(contour)
    print(f"area1 {area1}")
    if max<area1:
        
        max=area1
        imax=i
    if min>area1 and area1>2000:
        min=area1
        imin=i
print(min)
print(max)

contour=contours1[imax]
is_image = False # Flaga wskazująca czy na wejściem jest zdjęcie
# w formacie binarnym (True), czy kontur (False)
M = cv2.moments(contour, is_image)
cX = float(M["m10"] / M["m00"])
cY = float(M["m01"] / M["m00"])
print(f"center of mass ({cX}, {cY})")
contour=contours1[imin]
is_image = False # Flaga wskazująca czy na wejściem jest zdjęcie
# w formacie binarnym (True), czy kontur (False)
M = cv2.moments(contour, is_image)
cX2 = float(M["m10"] / M["m00"])
cY2 = float(M["m01"] / M["m00"])
print(f"center of mass ({cX2}, {cY2})")
X=cX-cX2
Y=cY-cY2
L=np.sqrt(X*X+Y*Y)
print("dlugość: "+str(L))

plt.figure()
plt.title("obraz oryginalny")
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.figure()
plt.title("kontury")
#  Rysowanie wszystkich konturów na czerwono, grubość linii 2
cv2.drawContours(img, contours1, imax, (255, 0, 0), 2)
cv2.drawContours(img, contours1, imin, (255, 0, 0), 2)
cv2.line(img, (int(cX), int(cY)), (int(cX2), int(cY2)), (255, 0, 0), 3)

imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.show()
print("koniec zad3")

#4
import cv2 # biblioteka OpenCV <- znakiem # oznaczamy początek komentarza
import numpy as np # biblioteka numpy (wywoływana pod nazwą np)
import matplotlib.pyplot as plt # biblioteka matplotlib moduł 
 # wyświetlania obrazów (plt)
sciezka_do_zdjecia = r"C:\Users\sebex\Desktop\Projekty\Python_Projects3\Project3\czesc_z_otworami_bw.tif" # ‘r’ tylko dla Windows
img = cv2.imread(sciezka_do_zdjecia) # format oryginalny
img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(img_gs, 40, 255, 0)
contours1, hierarchy1 = cv2.findContours(thresh, cv2.RETR_LIST,
 cv2.CHAIN_APPROX_NONE)


for i in range(0,len(contours1)-1):
    contour = contours1[i]
    area1 = cv2.contourArea(contour)
    print(f"area1 {area1}")
    


contour=contours1[0]
is_image = False # Flaga wskazująca czy na wejściem jest zdjęcie
# w formacie binarnym (True), czy kontur (False)
M = cv2.moments(contour, is_image)
cX = float(M["m10"] / M["m00"])
cY = float(M["m01"] / M["m00"])
print(f"center of mass ({cX}, {cY})")
is_closed = True # flaga wskazująca czy jest to zamknięty kontur (True), 
 # czy krzywa (False)
contour_length = cv2.arcLength(contour, is_closed)
d=contour_length/np.pi
print("pierwsza srednica: "+str(d))
contour=contours1[1]
is_image = False # Flaga wskazująca czy na wejściem jest zdjęcie
# w formacie binarnym (True), czy kontur (False)
M = cv2.moments(contour, is_image)
cX2 = float(M["m10"] / M["m00"])
cY2 = float(M["m01"] / M["m00"])
print(f"center of mass ({cX2}, {cY2})")
X=cX-cX2
Y=cY-cY2
L=np.sqrt(X*X+Y*Y)
print("dlugość: "+str(L))

is_closed = True # flaga wskazująca czy jest to zamknięty kontur (True), 
 # czy krzywa (False)
contour_length2 = cv2.arcLength(contour, is_closed)
d2=contour_length2/np.pi
print("druga srednica: "+str(d2))


plt.figure()
plt.title("obraz oryginalny")
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.figure()
plt.title("kontury")
#  Rysowanie wszystkich konturów na czerwono, grubość linii 2
cv2.drawContours(img, contours1, 0, (255, 0, 0), 2)
cv2.drawContours(img, contours1, 1, (255, 0, 0), 2)
cv2.line(img, (int(cX), int(cY)), (int(cX2), int(cY2)), (0, 255, 0), 3)
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.show()
print("koniec zad4")

#5
import cv2 # biblioteka OpenCV <- znakiem # oznaczamy początek komentarza
import numpy as np # biblioteka numpy (wywoływana pod nazwą np)
import matplotlib.pyplot as plt # biblioteka matplotlib moduł 
 # wyświetlania obrazów (plt)
sciezka_do_zdjecia = r"C:\Users\sebex\Desktop\Projekty\Python_Projects3\Project3\czesc_2_bw_bw.tif" # ‘r’ tylko dla Windows
img = cv2.imread(sciezka_do_zdjecia) # format oryginalny
img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(img_gs, 40, 255, 0)
contours1, hierarchy1 = cv2.findContours(thresh, cv2.RETR_LIST,
 cv2.CHAIN_APPROX_NONE)


for i in range(0,len(contours1)-1):
    contour = contours1[i]
    area1 = cv2.contourArea(contour)
    print(f"area1 {area1}")
    

contour = contours1[1]
area = cv2.contourArea(contour)
hull = cv2.convexHull(contour)

# contour=contours1[1]
# is_image = False # Flaga wskazująca czy na wejściem jest zdjęcie
# # w formacie binarnym (True), czy kontur (False)
# M = cv2.moments(contour, is_image)
# cX = float(M["m10"] / M["m00"])
# cY = float(M["m01"] / M["m00"])
# print(f"center of mass ({cX}, {cY})")
is_closed = True # flaga wskazująca czy jest to zamknięty kontur (True), 
#  # czy krzywa (False)
contour_length = cv2.arcLength(contour, is_closed)
d=contour_length/np.pi
print("pierwsza srednica: "+str(d))
# contour=contours1[1]
# is_image = False # Flaga wskazująca czy na wejściem jest zdjęcie
# # w formacie binarnym (True), czy kontur (False)
# M = cv2.moments(contour, is_image)
# cX2 = float(M["m10"] / M["m00"])
# cY2 = float(M["m01"] / M["m00"])
# print(f"center of mass ({cX2}, {cY2})")
# X=cX-cX2
# Y=cY-cY2
# L=np.sqrt(X*X+Y*Y)
# print("dlugość: "+str(L))

# is_closed = True # flaga wskazująca czy jest to zamknięty kontur (True), 
#  # czy krzywa (False)
# contour_length2 = cv2.arcLength(contour, is_closed)
# d2=contour_length2/np.pi
# print("druga srednica: "+str(d2))

ellipse = cv2.fitEllipse(contour)
center, axis, rotation = cv2.fitEllipse(contour)
print("axis: "+str(axis))
axisw=float(axis[1])
axism=float(axis[0])
wsp_smuklosci = axisw/axism
wsp_wypelnienia = area/area
wsp_zwartosci = (contour_length*contour_length)/(4*np.pi*area)
print("zwartosci: "+ str(wsp_zwartosci))
print("wypelnienia: "+ str(wsp_wypelnienia))
print("smuklosci: "+ str(wsp_smuklosci))
plt.figure()
plt.title("obraz oryginalny")
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.figure()
plt.title("kontury")
#  Rysowanie wszystkich konturów na czerwono, grubość linii 2
cv2.drawContours(img, contours1, 1, (255, 0, 0), 2)
cv2.ellipse(img, ellipse, (0, 0, 255), 3)
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.show()
print("koniec zad5")
#6
import cv2 # biblioteka OpenCV <- znakiem # oznaczamy początek komentarza
import numpy as np # biblioteka numpy (wywoływana pod nazwą np)
import matplotlib.pyplot as plt # biblioteka matplotlib moduł 
 # wyświetlania obrazów (plt)
sciezka_do_zdjecia = r"C:\Users\sebex\Desktop\Projekty\Python_Projects3\Project3\czesc_1_bw_bw.tif" # ‘r’ tylko dla Windows
img = cv2.imread(sciezka_do_zdjecia) # format oryginalny
img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(img_gs, 40, 255, 0)
contours1, hierarchy1 = cv2.findContours(thresh, cv2.RETR_LIST,
 cv2.CHAIN_APPROX_NONE)


for i in range(0,len(contours1)-1):
    contour = contours1[i]
    area1 = cv2.contourArea(contour)
    print(f"area1 {area1}")
    
contour = contours1[1]
area0 = cv2.contourArea(contour)
contour = contours1[2]
area1 = cv2.contourArea(contour)
# hull = cv2.convexHull(contour)
areakonc=area1-area0
hull = cv2.convexHull(contour)
areahull = cv2.contourArea(hull)
print("hull: "+str(areahull))
# contour=contours1[1]
# is_image = False # Flaga wskazująca czy na wejściem jest zdjęcie
# # w formacie binarnym (True), czy kontur (False)
# M = cv2.moments(contour, is_image)
# cX = float(M["m10"] / M["m00"])
# cY = float(M["m01"] / M["m00"])
# print(f"center of mass ({cX}, {cY})")
is_closed = True # flaga wskazująca czy jest to zamknięty kontur (True), 
#  # czy krzywa (False)
contour_length = cv2.arcLength(contour, is_closed)
d=contour_length/np.pi
print("pierwsza srednica: "+str(d))
contour=contours1[1]
is_image = False # Flaga wskazująca czy na wejściem jest zdjęcie
# w formacie binarnym (True), czy kontur (False)
M = cv2.moments(contour, is_image)
cX2 = float(M["m10"] / M["m00"])
cY2 = float(M["m01"] / M["m00"])
print(f"center of mass ({cX2}, {cY2})")
X=cX-cX2
Y=cY-cY2
L=np.sqrt(X*X+Y*Y)
print("dlugość: "+str(L))

is_closed = True # flaga wskazująca czy jest to zamknięty kontur (True), 
 # czy krzywa (False)
contour_length2 = cv2.arcLength(contour, is_closed)
d2=contour_length2/np.pi
print("druga srednica: "+str(d2))

ellipse = cv2.fitEllipse(contour)
center, axis, rotation = cv2.fitEllipse(contour)
print("axis: "+str(axis))
axisw=float(axis[1])
axism=float(axis[0])
wsp_powloki=areakonc/areahull
wsp_wypelnienia=areakonc/area1
wsp_smuklosci = axisw/axism
wsp_zwartosci = (contour_length*contour_length)/(4*np.pi*areakonc)
print("zwartosci: "+ str(wsp_zwartosci))
print("wypelnienia: "+ str(wsp_wypelnienia))
print("smuklosci: "+ str(wsp_smuklosci))
print("powloki: "+ str(wsp_powloki))
wsp_malinowskiej = contour_length/(2*np.sqrt(np.pi*areakonc)) - 1
print("malinowskiej: "+ str(wsp_malinowskiej))
plt.figure()
plt.title("obraz oryginalny")
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.figure()
plt.title("kontur")
#  Rysowanie wszystkich konturów na czerwono, grubość linii 2
cv2.drawContours(img, contours1, 1, (255, 0, 0), 2)
imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.figure()
plt.title("kontury")
#  Rysowanie wszystkich konturów na czerwono, grubość linii 2
cv2.drawContours(img, contours1, 1, (255, 0, 0), 2)
cv2.drawContours(img, contours1, 2, (255, 0, 0), 2)

imgplot = plt.imshow(img) # wyświetlanie zdjęcia w formacie rgb
plt.show()
print("koniec zad5")