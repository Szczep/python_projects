# This is a part of my diploma work. My goal is to classify person id by only ECG features so next step will be replacing data about persons age and sex
# by FFT spectrum features. First function is responsible for creating dataframe/csv file with persons features. The second one to create neural network 
#  and supervised classification.

# W tej części pracy założono cel odgadnięcia przez sieć neuronową osoby znajdującej się za kierownicą po zmierzonych cechach sygnału EKG tej osoby. 
# W pierwszej części jest tworzony plik w formacie csv zawierający cechy EKG oraz pewne cechy kierowcy. W drugiej części bazując na tych cechach model 
# sieci neuronowej uczy się i przewiduje, kto znajduje się za kierownicą pojazdu. W dalszej części będą podjęte starania oparcia uczenia się modelu 
# tylko na danych wyekstraktowanych z sygnału EKG.

import os
import PyCRC
from pyecg import ECGRecord
import wfdb
import pandas as pd
import statistics
import numpy as np
from scipy.signal import butter, lfilter, find_peaks
import neurokit2 as nk
import sklearn.model_selection
import sklearn.neural_network
import math
import re

catalog_dir=r"D:\ecg-id-database-1.0.0"
saving_data_dir=r"C:\Users\sebex\Desktop\Python_Projects\ECG_Classification_project/ECG_Classification_data2.csv"

def get_files(catalog_dir,saving_data_dir):
    """
    This function is creating csv file from catalogs with ECG signal data.
    catalog_dir is direction to input catalog with numerated catalogs with hea files.
    saving_data_dir is direction to place, where user want save csv file with ECG features.
    """
    file=open("error_list.txt","w") # a text file with error messages and wrong processed ECG signals/ plik tekstowy z błędami oraz przypisanymi im żle przetworzonymi sygnałami EKG
    Classification_ECG_data=pd.DataFrame(columns=["PR","RT","PR/RT","P","Q","R","S","T","Age","Sex","Person"])
    print(Classification_ECG_data)
    index=0
    for i in os.listdir(catalog_dir):
        person_nr=i[-1] if i[-2]=="0" else i[-2:]
        person=os.path.join(catalog_dir,i)
        for j in os.listdir(person):
            if j[-3:]=="hea":
                record = ECGRecord.from_wfdb(os.path.join(person,j))
                # lead_name=record.lead_names # signal names in .hea file/ nazwy sygnałów zawartych w pliku .hea
                labels=wfdb.rdheader(os.path.join(person,j)[:-4]) # exctracting sampling frequency, age and sex from hea file/ ekstraktowanie z pliku .hea częstotliwości próbkowania, wieku i płci badanego
                age=str(labels.comments[0]).replace("Age: ","")
                sex=str(labels.comments[1]).replace("Sex: ","")
                F_samp = labels.fs # number of samples/ ilosc probek na sekunde
                T = len(record)/(F_samp) # total time in seconds/ calkowity czas w sekundach T=n/fs
                dt = 1/F_samp # time step/ krok czasowy
                t = np.arange(0,T,dt) # time table/ tablica czasowa
                f = np.arange(0.01,F_samp/2,0.01) # frequency table/ tablica czestotliwosciowa
                signal = record.get_lead('I')

                #Cleaning data with bandpass filter/ Czyszczenie danych filtrem typu bandpass

                nyq=0.5*F_samp # signal frequency 2 times less than sampling frequency/ częstotliwość sygnału EKG 2 razy mniejsza od częstotliwośći próbkowania
                lowcut=1 # dla 0.5 i 0.05 sygnał "pływa", więcej dałoby "gładszy" charakter sygnału, ale zabrałoby część informacji
                highcut=40
                low = lowcut/nyq
                high = highcut/nyq
                b, a = butter(N=2, Wn=[low, high], btype='bandpass') # N - rząd filtru, a,b - składowe wektory współczynników
                y = lfilter(b, a, signal)
                #Finding R peaks/ Znajdowanie peaków R

                rpeaks, _=find_peaks(y,prominence=0.6) # the function is finding local maximums
                t_rpeaks=rpeaks/F_samp
                #Finding other characteristic peaks/ Odnajdywanie innych charakterystycznych peaków
                try:
                    _, waves_peak = nk.ecg_delineate(y, rpeaks, sampling_rate=F_samp, method="peak", show=False, show_type='peaks')
                    t_P_peaks=np.array(waves_peak['ECG_P_Peaks'])/F_samp
                    t_Q_peaks=np.array(waves_peak['ECG_Q_Peaks'])/F_samp
                    t_S_peaks=np.array(waves_peak['ECG_S_Peaks'])/F_samp
                    t_T_peaks=np.array(waves_peak['ECG_T_Peaks'])/F_samp
                except ValueError:
                    print("ValueError",print(i,j))
                    file.write("ValueError\n")
                    file.write(str(i))
                    file.write(str(j))
                    continue
                except ZeroDivisionError:
                    print("ZeroDivisionError",print(i,j))
                    file.write("ZeroDivisionError\n")
                    file.write(str(i))
                    file.write(str(j))
                    continue

                #Taking PR and RT intervals durations/ Ekstraktowanie czasu trawania odcinków PR i RT

                PR_intervals=[]
                RT_intervals=[]
                if waves_peak['ECG_P_Peaks'][0]<rpeaks[0]:
                    for i in range(len(rpeaks)):
                        PR_interval=rpeaks[i]-waves_peak['ECG_P_Peaks'][i]
                        PR_intervals.append(PR_interval)
                else:
                    for i in range(len(rpeaks)-1):
                        PR_interval=rpeaks[i+1]-waves_peak['ECG_P_Peaks'][i]
                        PR_intervals.append(PR_interval)
                if rpeaks[0]<waves_peak['ECG_T_Peaks'][0]:
                    for i in range(len(rpeaks)):
                        RT_interval=waves_peak['ECG_T_Peaks'][i]-rpeaks[i]
                        RT_intervals.append(RT_interval)
                else:
                    for i in range(len(rpeaks)-1):
                        RT_interval=waves_peak['ECG_P_Peaks'][i+1]-rpeaks[i]
                        RT_intervals.append(RT_interval)

                PR=statistics.median(PR_intervals)
                RT=statistics.median(RT_intervals)
                # P=statistics.median(list(y[waves_peak['ECG_P_Peaks']]).remove("None")) if "None" in list(y[waves_peak['ECG_P_Peaks']]) else statistics.median(y[waves_peak['ECG_P_Peaks']])
                # Q=statistics.median(list(y[waves_peak['ECG_Q_Peaks']]).remove("None")) if "None" in list(y[waves_peak['ECG_Q_Peaks']]) else statistics.median(y[waves_peak['ECG_Q_Peaks']])
                # R=statistics.median(list(y[rpeaks]).remove("None")) if "None" in list(y[rpeaks]) else statistics.median(y[rpeaks])
                # S=statistics.median(list(y[waves_peak['ECG_S_Peaks']]).remove("None")) if "None" in list(y[waves_peak['ECG_S_Peaks']]) else statistics.median(y[waves_peak['ECG_S_Peaks']])
                # T=statistics.median(list(y[waves_peak['ECG_T_Peaks']]).remove("None")) if "None" in list(y[waves_peak['ECG_T_Peaks']]) else statistics.median(y[waves_peak['ECG_T_Peaks']])
                try:
                    P=np.mean(y[waves_peak['ECG_P_Peaks']])
                except:
                    lenght=len(waves_peak['ECG_P_Peaks'])
                    try:
                        for ind in range(lenght):
                            if type(waves_peak['ECG_P_Peaks'][ind])!='numpy.int64':
                                del waves_peak['ECG_P_Peaks'][ind] 
                    except IndexError:
                        break
                    P=np.mean(y[waves_peak['ECG_P_Peaks']])
                try:
                    Q=np.mean(y[waves_peak['ECG_Q_Peaks']])
                except:
                    del waves_peak['ECG_Q_Peaks'][list(waves_peak['ECG_Q_Peaks']).index(np.nan)]
                    Q=np.mean(y[waves_peak['ECG_Q_Peaks']])
                try:
                    R=np.mean(y[rpeaks])
                except:
                    del rpeaks[list(rpeaks).index(np.nan)]
                    R=np.mean(y[rpeaks])
                try:
                    S=np.mean(y[waves_peak['ECG_S_Peaks']])
                except:
                    del waves_peak['ECG_S_Peaks'][list(waves_peak['ECG_S_Peaks']).index(np.nan)]
                    S=np.mean(y[waves_peak['ECG_S_Peaks']])
                try:
                    T=np.mean(y[list(waves_peak['ECG_T_Peaks'])])
                except:
                    # del waves_peak['ECG_T_Peaks'][list(waves_peak['ECG_T_Peaks']).index(np.nan)]
                    lenght=len(waves_peak['ECG_T_Peaks'])
                    try:
                        for ind in range(lenght):
                            if type(waves_peak['ECG_T_Peaks'][ind])!='numpy.int64':
                                del waves_peak['ECG_T_Peaks'][ind] 
                    except IndexError:
                        break
                    T=np.mean(y[waves_peak['ECG_T_Peaks']])
                
                # adding data to pandas dataframe/ dodawanie danych do dataframe

                Classification_ECG_data.loc[index,'PR'] = PR
                Classification_ECG_data.loc[index,'RT'] = RT
                Classification_ECG_data.loc[index,'Age'] = age
                Classification_ECG_data.loc[index,'Sex'] = sex
                Classification_ECG_data.loc[index,'Person'] = person_nr
                Classification_ECG_data.loc[index,"PR/RT"] = PR/RT if RT!=0 else 'NaN'
                Classification_ECG_data.loc[index,'P'] = P
                Classification_ECG_data.loc[index,'Q'] = Q
                Classification_ECG_data.loc[index,'R'] = R
                Classification_ECG_data.loc[index,'S'] = S
                Classification_ECG_data.loc[index,'T'] = T
                index+=1
    print(Classification_ECG_data)
    print(Classification_ECG_data["Person"].value_counts())
    Classification_ECG_data.dropna(inplace=True)
    file.close()
    Classification_ECG_data.to_csv(saving_data_dir)
    
def Machine_Learning_part(data_dir):
    """
    This function is including neural network to train and test ECG features data which is trained to predict persons id by these features
    """
    dt=pd.read_csv(data_dir)
    dt["Sex"]=dt["Sex"].apply(lambda x: 0 if x=="male" else 1)

    # Removing single ECG signals per person/ usuwanie pojednyczych sygnałów EKG przypadających na jedną osobę

    catalogs=[]
    for i in range(len(dt)):
        catalogs.append(dt['Person'].iloc[i])
    for i in range(len(catalogs)):
        if catalogs.count(catalogs[i])==1:
            dt.drop(dt.loc[dt["Person"]==catalogs[i]].index,axis=0,inplace=True)

    # X=dt.drop(["Sex","Age",'Person'],axis=1)
    X=dt.drop(["PR/RT","P","Q","R","S","T",'Person'],axis=1)
    # X=dt.drop(["P","Q","R","S","T",'Person'],axis=1)
    y=dt["Person"]

    # random splitting data to train and test data/ losowe dzielenie danych na uczące i testowe

    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X,y,test_size=0.2, random_state=40, shuffle=True)

    # supervised splitting data to train and test data giving oppurnity to train the model with minimum one ECG signal per person/
    # kontrolowane dzielenie danych na uczące i testowe dając możliwość nauki przynajmniej jednego sygnłu na osobę
    
    dt_copy=dt.copy()
    catalogs=[]
    for i in range(len(dt)):
        catalogs.append(dt['Person'].iloc[i])
    catalogs=list(dict.fromkeys(catalogs))
    dt_train=pd.DataFrame()
    dt_test=pd.DataFrame()
    for i in catalogs:
        dt_train=pd.concat([dt_train,dt_copy.loc[dt_copy["Person"]==i].head(1)], ignore_index=True) # min 1 signal per person to train data
        dt_copy.drop(dt_copy.loc[dt_copy["Person"]==i].head(1).index,axis=0,inplace=True)
        dt_test=pd.concat([dt_test,dt_copy.loc[dt_copy["Person"]==i].head(1)], ignore_index=True) # min 1 signal per person to test data
        dt_copy.drop(dt_copy.loc[dt_copy["Person"]==i].head(1).index,axis=0,inplace=True)
    X_train_sv=dt_train.drop("Person",axis=1) # supervised X and y splitting/ kontrolowany podział danych na X i y
    y_train_sv=dt_train["Person"]
    X_test_sv=dt_test.drop("Person",axis=1)
    y_test_sv=dt_test["Person"]
    X_sv=dt_copy.drop("Person",axis=1) # rest signals/ reszta sygnałów
    y_sv=dt_copy["Person"]
    X_train_rest, X_test_rest, y_train_rest, y_test_rest = sklearn.model_selection.train_test_split(X_sv,y_sv,test_size=0.2, random_state=40, shuffle=True) # random adding the rest of signals/ losowe dodawanie reszty sygnałów
    X_train_sv=pd.concat([X_train_sv,X_train_rest],ignore_index=True)
    X_test_sv=pd.concat([X_test_sv,X_test_rest],ignore_index=True)
    y_train_sv=pd.concat([y_train_sv,y_train_rest],ignore_index=True)
    y_test_sv=pd.concat([y_test_sv,y_test_rest],ignore_index=True)
    
    # Creating Multi Layer Perceptron Classifier neural network with two hidden layers, which have same size as output layer/
    # Tworzenie sieci neuronowej Multi Layer Perceptron Classifier o dwóch środkowych ukrytych warstwach o rozmiarze determinowanym rozmiarem wyjściowej warstwy

    MLPC=sklearn.neural_network.MLPClassifier(hidden_layer_sizes=(len(dt),len(dt),),max_iter=500, solver="adam") # lbfgs is more efficient for less amount of data/ lbfgs jest wydajniejsze dla małych danych
    MLPC.fit(X_train,y_train)
    y_pred=MLPC.predict(X_test) # predictind persons id by neural network/ przewidywanie id osób przez sieć neuronową
    score=MLPC.score(X_test,y_test)
    # print(len(MLPC.classes_))
    # print(y_pred)
    print(pd.DataFrame(y_pred,y_test))
    # print(dt["Person"].value_counts())
    print("Score of the trained randomly classifier is",score)
    # print(X.columns)

    MLPC_sv=sklearn.neural_network.MLPClassifier(hidden_layer_sizes=(len(dt),len(dt),),max_iter=500, solver="adam") # lbfgs is more efficient for less amount of data/ lbfgs jest wydajniejsze dla małych danych
    MLPC_sv.fit(X_train_sv,y_train_sv)
    y_pred_sv=MLPC_sv.predict(X_test_sv) # predictind persons id by neural network/ przewidywanie id osób przez sieć neuronową
    score_sv=MLPC_sv.score(X_test_sv,y_test_sv)
    print("Score of the trained under control classifier is",score_sv)
# get_files(catalog_dir,saving_data_dir)
Machine_Learning_part(saving_data_dir)

# clue: supervised splitting is giving better score than random splitting. These conditions are made for sytem, which is 
# measuring some drivers in the same car. First time the neural network is training during the first ride, after it's 
# training more or predicting, who is sitting in driver's seat.
# wniosek: kontrolowany podział danych dał możliwość uzyskania wyższego wyniku oceny modelu. Taki podział jest spowodowany
# warunkami w pojeździe podczas badania różnych kierowców za kierownicą, gdzie za pierwszym razem sieć neuronowa uczy się 
# sygnału zmierzonego na danej osobie, a następnie przy kolejnych podróżach może także się uczyć lub już przewidywać, kto 
# znajduje się w pojeździe na miejscu kierowcy