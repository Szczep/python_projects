import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
apples_kg_ordered = [2,4,7,3,13]
plums_kg_ordered = [3,8,9,1,1]
prices = [11.97 , 28.05, 38.98, 10.96, 41.1] # price for 2kg of apples and 3 kg of plums, 4kg apples+8kg plumes etc.
X=np.hstack((np.array(apples_kg_ordered).reshape(-1,1),np.array(plums_kg_ordered).reshape(-1,1)))
print(X)
y=np.array(prices).reshape(-1,1)
print(y)
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.4) # splitting data t
lr=LinearRegression()
lr.fit(X_train,y_train)
y_pred=lr.predict(X_test)
print('Two last predicted prices are:',y_pred) # predicted values by linear regression model
print('Efficiency of this linear regression model is:',round(lr.score(X_test,y_test),2)*100,'%') # comparising predicted prices with test prices
kg_of_apples=np.array([[1,0]])
print(kg_of_apples)
print('The price for 1 kg of apples is:',round(float(lr.predict(kg_of_apples)),2))
kg_of_plumps=np.array([[0,1]])
print(kg_of_plumps)
print('The price for 1 kg of plumps is:',round(float(lr.predict(kg_of_plumps)),2))

# with price for handbag 

potatoes_kg_ordered = [1,3,7,3,10,6,8,4,3,1,2,0]
tomatoes_kg_ordered = [5,2,3,1,2,3,6,7,3,2,3,1]
prices2 = [22.37 , 14.45, 26.6, 10.44, 28.49, 24.52, 40.38, 36.51, 18.50, 10.46, 16.51, 4.58] # price = x*potatoes + y*tomatoes + handbag

X2=np.hstack((np.array(potatoes_kg_ordered).reshape(-1,1),np.array(tomatoes_kg_ordered).reshape(-1,1)))
X2=np.hstack((X2,np.array(np.ones(len(potatoes_kg_ordered))).reshape(-1,1)))
print(X2)
y2=np.array(prices2).reshape(-1,1)
print(y2)
X2_train, X2_test, y2_train, y2_test = train_test_split(X2,y2,test_size=0.2) # splitting data t
lr2=LinearRegression()
lr2.fit(X2_train,y2_train)
y2_pred=lr2.predict(X2_test)
print('Three last predicted prices are:',y2_pred) # predicted values by linear regression model
print('Efficiency of this linear regression model is:',round(lr2.score(X2_test,y2_test),2)*100,'%') # comparising predicted prices with test prices
kg_of_potatoes=np.array([[1,0,0]])
print(kg_of_potatoes)
print('The price for 1 kg of potatoes is:',round(float(lr2.predict(kg_of_potatoes)),2))
kg_of_tomatoes=np.array([[0,1,0]])
print(kg_of_tomatoes)
print('The price for 1 kg of tomatoes is:',round(float(lr2.predict(kg_of_tomatoes)),2))
handbag=np.array([[0,0,1]])
print(handbag)
print('The price for 1 kg of tomatoes is:',round(float(lr2.predict(handbag)),2))

#without splitting

lr3=LinearRegression()
lr3.fit(X2,y2)
y3_pred=lr2.predict(X2)
print('Three last predicted prices are:',y2_pred) # predicted values by linear regression model
print('Efficiency of this linear regression model is:',round(lr2.score(X2,y2),2)*100,'%') # comparising predicted prices with test prices
kg_of_potatoes=np.array([[1,0,0]])
print(kg_of_potatoes)
print('The price for 1 kg of potatoes is:',round(float(lr3.predict(kg_of_potatoes)),2))
kg_of_tomatoes=np.array([[0,1,0]])
print(kg_of_tomatoes)
print('The price for 1 kg of tomatoes is:',round(float(lr3.predict(kg_of_tomatoes)),2))
handbag=np.array([[0,0,1]])
print(handbag)
print('The price for 1 handbag is:',round(float(lr3.predict(handbag)),2))
