
import os
import pandas as pd
import itertools

def find_endpoints(path,saving_path,output_file_name):

    files_list=os.listdir(path)
    files_paths_list=[]
    for i in files_list:
        files_paths_list.append(os.path.join(path,i))
    files_list_pd=pd.Series(files_list).to_frame() # creating data frame
    column=list(itertools.repeat('NaN',len(files_list_pd)))
    files_list_pd['Endpoints']=column
    files_list_pd.rename(columns={0:'Filenames'},inplace=True)
    endpoints=[]
    is_key=0
    for i in range(len(files_paths_list)):
        with open(files_paths_list[i],'r') as f:
            try: 
                for line in f.readlines():
                    print(line)
                    if is_key==1: 
                        endpoints=endpoints+list(line.split(' '))
                        is_key=0    
                    for word in list(line.split(' ')):
                        print(word,is_key)
                        if is_key==1: endpoints.append(word)
                        if word == "router.": is_key=1
                endpoints=str('/'.join(endpoints))
                files_list_pd.iloc[i,1]=endpoints
                endpoints=[]
            except UnicodeDecodeError:
                print(f.readline())
        f.close()
    print(files_list_pd)
    files_list_excel=files_list_pd.to_excel(saving_path+"\\"+output_file_name) # saving to file format .xlsx
path=r"C:\Users\sebex\Desktop\Projekty\Python_Projects3\Project2\files_to_search"
saving_path=r"C:\Users\sebex\Desktop\Projekty\Python_Projects3\Project2"
find_endpoints(path,path,"output.xlsx")
# file=input("Please type the directory to the catalog: ")
# saving_path=input("Please type the parent directory to results file: ")
# file_name=input("Please type file name with results: ")
# find_endpoints(path,saving_path,output_file_name)
